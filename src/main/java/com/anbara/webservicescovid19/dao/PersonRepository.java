package com.anbara.webservicescovid19.dao;

import com.anbara.webservicescovid19.entities.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource
public interface PersonRepository extends CrudRepository<Person,Long> {
}
