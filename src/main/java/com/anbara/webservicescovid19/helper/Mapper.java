package com.anbara.webservicescovid19.helper;

import com.anbara.webservicescovid19.dto.InputApiCovid;
import com.anbara.webservicescovid19.dto.OutputApiCovid;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

//@Service
public class Mapper {

    private Mapper() {
    }
/** This method convert list of InputApiCovid to object OutputApiCovid.
 * @param inputApiCovidList is list of object { date:string,casesToday:number }
 * @return  OutputApiCovid is object {  casesList:[],dateList:[] }
 * */
    public static OutputApiCovid convert(List<InputApiCovid> inputApiCovidList) {
        OutputApiCovid outputApiCovidList =new OutputApiCovid();
        List<Long> casesList = new ArrayList<>();
        List<LocalDate> dateList = new ArrayList<>();

        inputApiCovidList.forEach(item -> {
            casesList.add(item.getCasesToday());

//            String dateString = item.getDate();
//            dateString = dateString.substring(0, 10);

            StringBuilder dateString = item.getDate();
            dateString.delete(10,dateString.length());

            LocalDate localDate = LocalDate.parse(dateString);
            dateList.add(localDate);
        });
        outputApiCovidList.setCasesList(casesList);
        outputApiCovidList.setDateList(dateList);

        return outputApiCovidList;
    }
}
