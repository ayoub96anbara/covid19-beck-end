package com.anbara.webservicescovid19;

import com.anbara.webservicescovid19.dao.PersonRepository;
import com.anbara.webservicescovid19.entities.Person;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebServicesCovid19Application implements CommandLineRunner {

    private final PersonRepository personRepository;

    public WebServicesCovid19Application(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(WebServicesCovid19Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
       personRepository.save(new Person(null,"ayoub","anbara","eljadida"));
    }
}
