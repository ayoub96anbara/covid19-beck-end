package com.anbara.webservicescovid19.web;

import com.anbara.webservicescovid19.dto.OutputApiCovid;
import static com.anbara.webservicescovid19.helper.Mapper.convert;
import com.anbara.webservicescovid19.services.CallApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

@CrossOrigin("*")
@RestController
public class APIController {

    private final CallApiService apiService;
    private final Logger logger= LoggerFactory.getLogger(APIController.class);

    public APIController(CallApiService apiService) {
        this.apiService = apiService;
    }

    /** This method return object OutputApiCovid.
     * @return  OutputApiCovid is object {  casesList:[],dateList:[] }
     * */
    @GetMapping("/get")
    public OutputApiCovid getData() throws URISyntaxException {
        logger.info("execution getData()");
        return  convert(apiService.callApi());

    }
}
