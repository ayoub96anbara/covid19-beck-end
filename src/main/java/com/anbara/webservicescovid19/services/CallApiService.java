package com.anbara.webservicescovid19.services;

import com.anbara.webservicescovid19.dto.InputApiCovid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@Service
public class CallApiService {

    private final RestOperations restOperations;
    @Value("${url.api}")
    private  String URL_API;


    public CallApiService(RestOperations restOperations) {
        this.restOperations = restOperations;
    }

    public List<InputApiCovid> callApi() throws URISyntaxException {
        URI uri=new URI(URL_API);
        ResponseEntity<InputApiCovid[]> forEntity=
                restOperations.getForEntity(uri, InputApiCovid[].class);
        InputApiCovid[] inputApiCovid=forEntity.getBody();
        if (inputApiCovid==null) throw new NullPointerException("forEntity.getBody() is null");
        return Arrays.asList(inputApiCovid) ;

    }

}
