package com.anbara.webservicescovid19.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data @AllArgsConstructor @NoArgsConstructor
public class OutputApiCovid implements Serializable {

    private List<Long> casesList;
    private List<LocalDate> dateList;

}
