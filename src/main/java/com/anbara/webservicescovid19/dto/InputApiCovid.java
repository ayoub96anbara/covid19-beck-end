package com.anbara.webservicescovid19.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor @AllArgsConstructor @Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InputApiCovid implements Serializable {


//    @JsonProperty("Date")
//    private String date;

        @JsonProperty("Date")
        private StringBuilder date;


    @JsonProperty("Cases")
    private long casesToday;

}
